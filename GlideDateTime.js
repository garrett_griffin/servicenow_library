/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var GlideDateTime = Class.create({
    addSeconds: function() { },
    addDays: function() { },
    addWeeks: function() { },
    addMonths: function() { },
    addYears: function() { },
    hasDate: function() { },
    getUserTimeZone: function() { },
    setTZ: function(tz) { },
    setValue: function(Object) { },
    setValueUTC: function(dt, format) { },
    setGlideDateTime: function (GlideDateTime) { },
    isDST: function() { },
    getDSTOffset: function() { },
    setDisplayValueInternal: function(value) { },
    setDisplayValueInternalWithAlternates: function(value) { },
    setDisplayValue: function(value, format) { },
    getDisplayValue: function() { },
    getDisplayValueInternal: function() { },
    getValue: function() { },
    compareTo: function(o) { },
    getDayOfWeek: function() { },
    getDayOfWeekLocalTime: function() { },
    getDayOfWeekUTC: function() { },
    getDayOfYearLocalTime: function() { },
    getDayOfYearUTC: function() { },
    getWeekOfYearLocalTime: function() { },
    getWeekOfYearUTC: function() { },
    getSpanTime: function(int) { },
    getDayOfMonth: function() { },
    getDayOfMonthLocalTime: function() { },
    getDayOfMonthUTC: function() { },
    setDayOfMonth: function(day) { },
    setDayOfMonthLocalTime: function(day) { },
    setDayOfMonthUTC: function(day) { },
    getMonthLocalTime: function() { },
    getMonthUTC: function() { },
    getLocalDate: function() { },
    getUTCDate: function() { },
    getByFormat: function() { }
    
    
});

