/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var GlideElementPhoneNumber = Class.create();
GlideElementPhoneNumber.prototype = {
    getValue: function(){},
    setPhoneNumber: function(value, strict){},
    getDisplayValue: function(){},
    getGlobalDisplayValue: function(){},
    getLocalDisplayValue: function(){},
    isLocalFollowsGlobal: function(){},
    getUsersExitCode: function(){},
    getUsersPhoneFormat: function(){},
    getPhoneFormatForUser: function(userSysId){},
    getPhoneFormatForLocation: function(locationSysId){},
    getPhoneNumberFormat: function(phoneNumber){},
    getPhoneTerritories: function(){},
    getTerritory: function(){},
    getUsersTerritory: function(){},
    getTerritoryForUser: function(userSysId){},
    getGlobalDialingCode: function(){},
    getLocalDialingCode: function(){},
    getLocalDialingCodeOptionalText: function(){},
    isValid: function(){},
    isStrict: function(){},
    setStrict: function(strict){},
    getPartialMatchType: function(){},
    setAllowNationalEntry: function(allowNationalEntry){},
    getPhoneFormatForTerritory: function(territorySysId){},
    setPhoneNumberFormat: function(phoneNumberFormat){}
};