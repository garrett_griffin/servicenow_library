Author: Garrett Griffin
Contact: Garrett.Griffin@fruitionpartners.com


Use these Service-Now Library Files to help with auto-complete in the IDE of your choice.

I recommend Netbeans (netbeans.org).


Instructions for setting up Library Files in Netbeans:
1. Install Netbeans and set up a project for your Service-Now work.
2. Extract the archive and place the "Library" folder within your project.
3. Go to File -> Project Properties.
4. Click on PHP Include Path.
5. Click Add Folder.
6. Navigate to the Library folder and add it as an include path.
7. Restart Netbeans.


Once it finishes scanning the project, you should be able to us autocomplete in any JS file.
To try it out, create a new JS file, and type "gs." Once you type the period, it should pull up a list
of options that you can use to complete your code.