/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



//var Packages.com.glide.db.MultipleUpdate = Class.create();

//Packages.com.glide.db.MultipleUpdate.prototype = {
    
//};
var GlideMultipleUpdate = Class.create();
GlideMultipleUpdate.prototype = {
    DBI: new Object(),
    addGroupBy: function(){},
    addNotNullQuery: function(){},
    addNullQuery: function(){},
    addOrderBy: function(){},
    addQuery: function(){},
    addQueryCondition: function(){},
    addQueryString: function(){},
    addReturnField: function(){},
    addSetQuery: function(){},
    batchable: true,
    changeValue: function(){},
    close: function(){},
    equals: function(){},
    execute: function(){},
    executeAndReturnException: function(){},
    getClass: function(){},
    getDBI: function(){},
    getOperation: function(){},
    getPrimaryKeyValue: function(){},
    getTableName: function(){},
    getUpdateCount: function(){},
    getUpdateMap: function(){},
    getValue: function(){},
    getValueMap: function(){},
    hashCode: function(){},
    isBatchable: function(){},
    isNoReplication: function(){},
    noReplication: true,
    notify: function(){},
    notifyAll: function(){},
    operation: new Object(),
    primaryKeyValue: new Object(),
    setIncrement: function(){},
    setLimit: function(){},
    setNoReplicate: function(){},
    setTableName: function(){},
    setValue: function(){},
    tableName: new Object(),
    toString: function(){},
    updateCount: 12,
    valueMap: new Object(),
    wait: function(){}
};