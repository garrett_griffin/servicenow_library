/**
 * Created by Garrett on 4/22/2015.
 */
var GlideSession = Class.create();
GlideSession.prototype = {
    empty: function(){},
    bottom: function(){},
    moveTo: function(){},
    toString: function(){},
    clearCategoryParms: function(){},
    wait: function(){},
    clearCategoryParm: function(){},
    top: function(){},
    size: function(){},
    getClass: function(){},
    peek: function(){},
    setCategoryParm: function(){},
    popEntry: function(){},
    hashCode: function(){},
    class: new Object(),
    notify: function(){},
    savedParams: new Object(),
    hasMoreThanOne: function(){},
    isEmpty: function(){},
    saveEntry: function(){},
    getStackParm: function(){},
    getCategoryParms: function(){},
    purge: function(){},
    matchSavedEntry: function(){},
    equals: function(){},
    back: function(){},
    prev: function(){},
    pop: function(){},
    clear: function(){},
    realSize: function(){},
    getSavedParams: function(){},
    dump: function(){},
    push: function(){},
    getCategoryParm: function(){},
    notifyAll: function(){},
    get: function(){},
    setStackParm: function(){},
    hasOnlyOne: function(){},
    getStackParms: function(){},
    search: function(){},
    markClear: function(){}

};