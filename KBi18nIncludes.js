/*! RESOURCE: scripts/classes/KBSearch.js */
var KBSearch = Class.create();
KBSearch.prototype = {
    initialize: function() {
        this.expandFunction = this._advancedExpand.bind(this);
        this.collapseFunction = this._advancedCollapse.bind(this);
    },
    submit: function() {
        var form = gel('kb_view');
        this._setValues(form);
        return this._knowledgeSubmit(form);
    },
    toggleAdvanced: function() {
        var d = gel('adv_opts1');
        this._setToggleFunction(d);
        this.toggle(document.getElementById('adv_opts1'));
        this.toggle(document.getElementById('adv_opts2'));
        this.toggle(document.getElementById('adv_header1'));
        this.toggle(document.getElementById('adv_header2'));
    },
    _setToggleFunction: function(element) {
        if (element.style.display == "inline") {
            setPreference("kb.advanced.search", "none");
            this.toggle = this.collapseFunction;
        } else {
            setPreference("kb.advanced.search", "inline");
            this.toggle = this.expandFunction;
        }
    },
    _advancedExpand: function(el) {
        var w = getWidth(el);
        el.style.overflow = "hidden";
        el.style.display = "inline";
        el.style.width = "1px";
        new Rico.Effect.Size( el.id, w, null, 70, 14, {
            complete:function() {
                el.style.overflow = "";
            }
        });
    },
    _advancedCollapse: function(el) {
        var width = el.offsetWidth;
        el.style.overflow = "hidden";
        if (isMSIE) {
            new Rico.Effect.Size( el.id, 1, null, 50, 12, {
                complete:function() {
                    el.style.display = "none";
                    el.style.overflow = "";
                    el.style.width = width;
                }
            });
        } else {
            el.style.display = "none";
        }
    },
    _knowledgeSubmit: function(f) {
        var s = $('sysparm_search');
        var d = $('search_engine');
        this._setValues(f);
        var topic = f.sysparm_topic.value;
        var category = f.sysparm_category.value;
        var u_subcategory = f.sysparm_u_subcategory.value;
        if (s.value.length == 0 || !s.value) {
            f.action = "kb_list.do";
            if (topic && topic.length != 0)
                addInput(f, 'HIDDEN', 'jvar_view_topic', topic);
            if (category && category.length != 0 )
                addInput(f, 'HIDDEN', 'jvar_view_category', category);
            if (u_subcategory && u_subcategory.length != 0 )
                addInput(f, 'HIDDEN', 'jvar_view_u_subcategory', u_subcategory);
            return true;
        } else if (!d || d.options.selectedIndex < 1) {
            return true;
        } else {
            var o = d.options[d.options.selectedIndex];
            var f = "script_" + o.value;
            var text = s.value;
            eval(f + "(text)");
        }
        return false;
    },
    _setValues: function (f) {
        f['sysparm_topic'].value = f['kb_knowledge.topic'].value;
        f['sysparm_category'].value = f['kb_knowledge.category'].value;
        f['sysparm_u_subcategory'].value = f['kb_knowledge.u_subcategory'].value;
    },
    verifySearchEngine: function(kbname) {
        var d = $('search_engine');
        if (d && d.options.selectedIndex > 0) {
            var topic = d.form['kb_knowledge.topic'];
            if (topic && topic.options.selectedIndex > 0)
                alert("Category searches are only available in " + kbname);
        }
    },
    resetAdvanced: function() {
        var s = $('sysparm_search');
        var topic = s.form['kb_knowledge.topic'];
        topic.options.selectedIndex = 0;
        if (topic['onchange'])
            topic.onchange();
        var e = $('search_engine');
        gel('kb_knowledge.topic').disabled = e.value != '';
        gel('kb_knowledge.category').disabled = e.value != '';
        gel('kb_knowledge.u_subcategory').disabled = e.value != '';
    }
}
/*! RESOURCE: scripts/classes/KBI18nSearch.js */
var KBI18nSearch = Class.create(KBSearch,  {
    toggleAdvanced: function() {
        var d = gel('kb_advanced_search');
        this._setToggleFunction(d);
        this.toggle(d);
    },
    _advancedExpand: function(element) {
        expandEffect(element);
    },
    _advancedCollapse: function(element) {
        collapseEffect(element);
    },
    _knowledgeSubmit: function(f) {
        var s = $('sysparm_search');
        if (s.value.length == 0 || !s.value) {
            if (f.sysparm_language) {
                var language = f.sysparm_language.value;
                if (language && language.length != 0)
                    addInput(f, 'HIDDEN', 'jvar_view_language', language);
            }
            if (f.sysparm_department) {
                var department = f.sysparm_department.value;
                if (department && department.length != 0)
                    addInput(f, 'HIDDEN', 'jvar_view_department', department);
            }
        }
        return KBSearch.prototype._knowledgeSubmit.call(this, f);
    },
    _setValues: function(f) {
        KBSearch.prototype._setValues.call(this, f);
        if (f['sysparm_language'])
            f['sysparm_language'].value = f['kb_language_filter_select'].value;
        if (f['sysparm_department']) {
            f['sysparm_department'].value = f['kb_department'].value;
            f['sysparm_department_display'].value = f['sys_display.kb_department'].value;
        }
    },
    _setToggleFunction: function(element) {
        if (element.style.display == "block") {
            setPreference("kb.advanced.search", "none");
            this.toggle = this.collapseFunction;
        } else {
            setPreference("kb.advanced.search", "block");
            this.toggle = this.expandFunction;
        }
    }
});
